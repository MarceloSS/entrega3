const TotalPrice = ({list}) => {
    const prices = []
    list.map(fruit => prices.push(fruit.price))

    const sum = (total, num) =>{
        return total + num
    }

    return(
        <li>{prices.reduce(sum)}</li>
    )
}

export default TotalPrice