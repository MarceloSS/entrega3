import React from 'react';
import Fruta from './Fruta';
import TotalPrice from './TotalPrice'

const App = () => {

  const fruits = [
        { name: "banana", cor: "yellow", price: 2 },
        { name: "cherry", cor: "red", price: 3 },
        { name: "strawberry", cor: "red", price: 4 },
      ]

  return (
    <div className="App" >
      <div id="all fruit names">{fruits.map((fruta, index) => <Fruta fruit={fruta} key={index}/>)} </div>
      <div id="red fruit names">{fruits.filter(fruta => fruta.cor == "red").map((fruta,index) => <Fruta fruit={fruta} key={index}/>) }</div>
      <div id="total"><TotalPrice list={fruits}/></div>
    </div>
  );
}

export default App